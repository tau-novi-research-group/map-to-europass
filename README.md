# ↦ Europass

A compiler for transforming non-Europass-compliant digital credentials into a
form which the EDCI or EDCL framework recognises. In other words, the results
of the compilation conform to the [Europass Learning Model].

[Europass Learning Model]: https://github.com/european-commission-europass/Europass-Learning-Model
